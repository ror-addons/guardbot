GuardBot = {}

local GetBuffs = GetBuffs

local timeDelay = 2           --throttle for the Update() process
local timeLeft = timeDelay    --amount of time (seconds) left before doing another Update() process

GuardBot.isActive = false        --is the addon active (valid tank career found)?
GuardBot.isAttached = false      --is the icon attached to a world object?
GuardBot.guardActive = false     --are we currently guarding someone?
GuardBot.guardedPlayerName = L"" --the name of the player being guarded
GuardBot.guardedPlayerId = 0     --entity id of the player being guarded
GuardBot.guardAbilityId = 0      --the id of the class-specific Guard ability (matches one of the ids in the guardList table)
GuardBot.needUpdate = false

GuardBot.Settings = {
	iconOffsetX = 0,
	iconOffsetY = 0,
	iconTexScaleMult = 1.5,
	
	iconColorR = 255,
	iconColorG = 255,
	iconColorB = 255,
	iconAlpha  = 1.0,
	
	enablePartyMsgs = false,
	
	defaultIconOffsetX = 0,
	defaultIconOffsetY = 0,
	defaultIconTexScaleMult = 1.5,
	
	defaultIconColorR = 255,
	defaultIconColorG = 255,
	defaultIconColorB = 255,
	defaultIconAlpha  = 1.0,
	
	defaultEnablePartyMsgs = false,
}

--list of Guard ability ids for each tank class
local guardList = {
	[ 1] = 1363;	-- Ironbreaker;
	[10] = 8013;	-- KotBS;
	[17] = 9008;	-- Swordmaster;
	[21] = 9325;	-- Blackguard
	[13] = 8325;	-- Chosen
	[ 5] = 1674;    -- Black Orc
};

function GuardBot.Initialize()
	RegisterEventHandler (SystemData.Events.PLAYER_BEGIN_CAST, "GuardBot.OnPlayerBeginCast")
	RegisterEventHandler (SystemData.Events.PLAYER_END_CAST, "GuardBot.OnPlayerEndCast")
	RegisterEventHandler (SystemData.Events.PLAYER_CAREER_LINE_UPDATED, "GuardBot.OnCareerLineUpdated")
	RegisterEventHandler (SystemData.Events.INTERFACE_RELOADED, "GuardBot.OnCareerLineUpdated")
	RegisterEventHandler (SystemData.Events.LOADING_END, "GuardBot.OnCareerLineUpdated")
	RegisterEventHandler (SystemData.Events.PLAYER_DEATH, "GuardBot.OnPlayerDeath")
	RegisterEventHandler (SystemData.Events.PLAYER_TARGET_UPDATED, "GuardBot.OnPlayerTargetUpdated")
	
	d("GuardBot Initialized!");
end

function GuardBot.Shutdown()
	UnregisterEventHandler (SystemData.Events.PLAYER_BEGIN_CAST, "GuardBot.OnPlayerBeginCast")
	UnregisterEventHandler (SystemData.Events.PLAYER_END_CAST, "GuardBot.OnPlayerEndCast")
	UnregisterEventHandler (SystemData.Events.PLAYER_CAREER_LINE_UPDATED, "GuardBot.OnCareerLineUpdated")
	UnregisterEventHandler (SystemData.Events.INTERFACE_RELOADED, "GuardBot.OnCareerLineUpdated")
	UnregisterEventHandler (SystemData.Events.LOADING_END, "GuardBot.OnCareerLineUpdated")
	UnregisterEventHandler (SystemData.Events.PLAYER_DEATH, "GuardBot.OnPlayerDeath")
	UnregisterEventHandler (SystemData.Events.PLAYER_TARGET_UPDATED, "GuardBot.OnPlayerTargetUpdated")
end

--Iterates through the tank's group looking for the guarded player. If found, the icon window is attached to that player. 
function GuardBot.Update(elapsedTimeMs)
	--short circuit the update loop if possible
	if GuardBot.isActive == false or GuardBot.guardActive == false then return end	

	timeLeft = timeLeft - elapsedTimeMs
	if timeLeft > 0 then return end --not ready to do another Update() process
	
	timeLeft = timeDelay		
	
	--check to make sure we're in a group and Guard is active on the tank
	if GuardBot.GetGuardBuffStatus() == true then
		local groupMemberEntityId = 0
		local groupMemberName = L""
		
		for i = 1, table.getn(GetGroupData()) do
			groupMemberEntityId = GetGroupData()[i].worldObjNum
			groupMemberName = GetGroupData()[i].name
			groupMemberName = towstring(groupMemberName)
			groupMemberName = groupMemberName:match(L"([^^]+)^?([^^]*)")
	
			if groupMemberName == GuardBot.guardedPlayerName then
				--this can happen if the player goes WAY out of range of the guarded player
				--the API will return an entity id of 0 for this group member so we hide the icon to prevent it from 'sticking'
				--on the screen
				--the icon will become visible again when the tank and the guarded player are in range of each other
				if groupMemberEntityId == 0 or groupMemberEntityId == nil then
					WindowSetShowing("GuardBotIconWindow", false)
					WindowSetShowing("GuardBotIconWindowIcon", false)
					GuardBot.isAttached = false
					
					d("GuardBot Error - Guarded group member has an invalid entity ID!")
					return
				end
				
				GuardBot.guardedPlayerId = groupMemberEntityId
				GuardBot.AttachIconWindow()
			end
		end
	else
		GuardBot.ResetGuardData()
	end
end

--Ensures the addon only functions while the player is playing a tank class.		
function GuardBot.OnCareerLineUpdated()
	if GameData.Player.career.line == GameData.CareerLine.SWORDMASTER
	or GameData.Player.career.line == GameData.CareerLine.BLACK_ORC
	or GameData.Player.career.line == GameData.CareerLine.CHOSEN
	or GameData.Player.career.line == GameData.CareerLine.IRON_BREAKER
	or GameData.Player.career.line == GameData.CareerLine.KNIGHT
	or GameData.Player.career.line == GameData.CareerLine.SHADE then --Blackguard, I hope?
		GuardBot.isActive = true
	else
		GuardBot.isActive = false
		GuardBot.guardAbilityId = 0
		
		return
	end
	
	GuardBot.SetupIconWindow()
		
	--if we get here, the player is a tank so let's find the appropriate Guard ability id for this class
	GuardBot.guardAbilityId = guardList[GameData.Player.career.line]
	
	--register /guardbot command
	if LibSlash and not LibSlash.IsSlashCmdRegistered("guardbot") then
		LibSlash.RegisterSlashCmd("guardbot", GuardBot.HandleSlashCmds)
	end
end

--Immediately reset addon (hide icon, etc.) if the tank dies.
function GuardBot.OnPlayerDeath()
	GuardBot.ResetGuardData()
end

--Force an update to the TargetInfo object when the player's target changes.
function GuardBot.OnPlayerTargetUpdated(targetClass, targetId, targetType)
	if GuardBot.isActive == false or GuardBot.guardActive == false then
		return
	end
	
	if targetClass ~= "selffriendlytarget" or targetClass ~= "mouseovertarget" then
		return
	end
	
	TargetInfo:UpdateFromClient()
	
	if(TargetInfo:UnitName(targetClass)):match(L"([^^]+)^?([^^]*)") == GuardBot.guardedPlayerName then
		GuardBot.guardedPlayerId = TargetInfo:UnitEntityId(targetClass)
		GuardBot.isAttached = false
		GuardBot.AttachIconWindow()
	end
end

function GuardBot.OnPlayerBeginCast(actionId, isChannel, desiredCastTime, averageLatency)
	if GuardBot.isActive == false then
		return
	end
	
	if GuardBot.guardAbilityId == actionId then
		GuardBot.needUpdate = true
	end
end

function GuardBot.OnPlayerEndCast(failed)
	if GuardBot.needUpdate == true then
		--Guard failed to cast for some reason
		if failed == true then
			GuardBot.ResetGuardData()
			--[[
			--check to see if Guard is on the tank (it shouldn't be)
			if GuardBot.GetGuardBuffStatus() == false then
				--Guard is not on the tank so everything needs to be reset
				GuardBot.ResetGuardData()
			else
				--if we get here, GuardBot is desycned from the player
				--there's no way to recover from this, best to dump the error to the debug log and reset everything
				--the player can resync GuardBot by casting Guard again
				d("GuardBot Error: GuardBot is desycned!")
				GuardBot.ResetGuardData()
			end
			--]]
		else
			--Guard was cast successfully
			GuardBot.guardedPlayerName = towstring(TargetInfo:UnitName("selffriendlytarget"))
			GuardBot.guardedPlayerName = GuardBot.guardedPlayerName:match(L"([^^]+)^?([^^]*)")
			GuardBot.guardedPlayerId = TargetInfo:UnitEntityId("selffriendlytarget")
			
			GuardBot.AttachIconWindow()
			
			if GuardBot.Settings.enablePartyMsgs == true then
				local text = towstring("I'm guarding you "..tostring(GuardBot.guardedPlayerName).."!")
				
				if GameData.Player.isInScenario == true then
					SystemData.UserInput.ChatChannel = L"/sp"
				else
					SystemData.UserInput.ChatChannel = L"/p"
				end
				
				SystemData.UserInput.ChatText = towstring(text)
				
				BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
			end
		end
	end
end


function GuardBot.AttachIconWindow()
	--make sure the icon isn't already attached
	if GuardBot.isAttached == false then
		AttachWindowToWorldObject("GuardBotIconWindow", GuardBot.guardedPlayerId)
		WindowSetShowing("GuardBotIconWindow", true)
		WindowSetShowing("GuardBotIconWindowIcon", true)
		
		GuardBot.isAttached = true
		GuardBot.guardActive = true
	end
	
	GuardBot.needUpdate = false
end

function GuardBot.SetupIconWindow()
	WindowSetScale("GuardBotIconWindow", GuardBot.Settings.iconTexScaleMult)
	WindowClearAnchors("GuardBotIconWindowIcon")
	WindowAddAnchor("GuardBotIconWindowIcon", "center", "GuardBotIconWindow", "center", GuardBot.Settings.iconOffsetX, GuardBot.Settings.iconOffsetY) --(GuardBot.Settings.iconOffsetX / InterfaceCore.GetScale()), (GuardBot.Settings.iconOffsetY / InterfaceCore.GetScale()))

	WindowSetTintColor("GuardBotIconWindowIcon", GuardBot.Settings.iconColorR, GuardBot.Settings.iconColorG, GuardBot.Settings.iconColorB)
	WindowSetAlpha("GuardBotIconWindowIcon", GuardBot.Settings.iconAlpha)
end

function GuardBot.ResetGuardData()
	if GuardBot.isAttached == true then
		DetachWindowFromWorldObject("GuardBotIconWindow", GuardBot.guardedPlayerId)
	end
	
	WindowSetShowing("GuardBotIconWindow", false)
	WindowSetShowing("GuardBotIconWindowIcon", false)
	
	GuardBot.guardedPlayerName = L""
	GuardBot.guardedPlayerId = 0
	GuardBot.guardActive = false
	GuardBot.isAttached = false
	GuardBot.needUpdate = false
end

--Returns 1 if Guard buff is active on the tank, 0 otherwise
function GuardBot.GetGuardBuffStatus()
	local buffs = GetBuffs (GameData.BuffTargetType.SELF)
	for index, buffData in pairs(buffs) do		
		if buffData.abilityId == GuardBot.guardAbilityId and buffData.castByPlayer == true then	
			return true
		end
	end
	
	return false			
end

--Translates slash commands into something usable. Slash commands take the form of: /guardbot <command> [param1 param2 param3 ...]
function GuardBot.HandleSlashCmds(input)
	--if there's no input given, then remind the user of the settings
	if string.find(input, "%S") == nil then
		GuardBot.ShowHelp()
		return
	end
	
	--there's input, so format it first
	input = string.lower(input) --convert to lowercase
	input = string.gsub(input, "(%s+)", " ") --condense multiple spaces between a command into one	
	
	local command = string.match(input, "(%w+)=?")
	local value = string.sub(input, string.len(command) + 2, string.len(input))
	
	--perform the command if possible
	if command == "help" then
		GuardBot.ShowHelp()

		return
	elseif command == "showsettings" then
		GuardBot.DumpSettings()

		return

	elseif command == "disable" then
		GuardBot.ResetGuardData()
		GuardBot.isActive = false
		
		GuardBot.PrintToChat("GuardBot disabled!", 4)

		return

	elseif command == "enable" then
		GuardBot.ResetGuardData()
		GuardBot.OnCareerLineUpdated()
		
		GuardBot.PrintToChat("GuardBot enabled!", 4)

		return
	
	elseif command == "resetsettings" then
		GuardBot.Settings.iconOffsetX = GuardBot.Settings.defaultIconOffsetX
		GuardBot.Settings.iconOffsetY = GuardBot.Settings.defaultIconOffsetY
		GuardBot.Settings.iconTexScaleMult = GuardBot.Settings.defaultIconTexScaleMult
		GuardBot.Settings.iconColorR = GuardBot.Settings.defaultIconColorR
		GuardBot.Settings.iconColorG = GuardBot.Settings.defaultIconColorG
		GuardBot.Settings.iconColorB = GuardBot.Settings.defaultIconColorB
		GuardBot.Settings.iconAlpha = GuardBot.Settings.defaultIconAlpha
		GuardBot.Settings.enablePartyMsgs = GuardBot.Settings.defaultEnablePartyMsgs
		
		GuardBot.PrintToChat("GuardBot settings have been reset to default.", 4)
		
		return

	elseif command == "setoffset" and value ~= nil then
		--grab the two offsets (in format: (X Y))
		local offsets = GuardBot.SplitSlashParam(value, " ")
		offsets[1] = tonumber(offsets[1])
		offsets[2] = tonumber(offsets[2])
		
		if offsets[1] == nil then offsets[1] = GuardBot.Settings.iconOffsetX end
		if offsets[2] == nil then offsets[2] = GuardBot.Settings.iconOffsetY end
		
		GuardBot.Settings.iconOffsetX = offsets[1]
		GuardBot.Settings.iconOffsetY = offsets[2]
		
		GuardBot.SetupIconWindow()
		
		GuardBot.PrintToChat("GuardBot icon offset has been changed.", 4)
		
		return

	elseif command == "setscale" and value ~= nil then
		local scale = tonumber(value)
		if scale <= 0 or scale == nil then
			GuardBot.PrintToChat("GuardBot Error: "..value.." is not a valid icon scale. Must be greater than equal to 0.", 3)
			return
		else
			GuardBot.Settings.iconTexScaleMult = scale
			
			GuardBot.SetupIconWindow()
			GuardBot.PrintToChat("GuardBot icon scale has been changed.", 4)
		end
		
		return
	
	elseif command == "setcolor" and value ~=nil then
		local color = GuardBot.SplitSlashParam(value, " ")
		color[1] = tonumber(color[1])
		color[2] = tonumber(color[2])
		color[3] = tonumber(color[3])
		
		if color[1] == nil or color[1] > 255 or color[1] < 0 then
			GuardBot.PrintToChat("GuardBot Error: "..color[1].." is not a valid color component. Value must be greater than or equal to 0 and less than 256.", 3)
			
			return
		end
		
		if color[2] == nil or color[2] > 255 or color[2] < 0 then
			GuardBot.PrintToChat("GuardBot Error: "..color[2].." is not a valid color component. Value must be greater than or equal to 0 and less than 256.", 3)		
		
			return
		end
		
		if color[3] == nil or color[3] > 255 or color[3] < 0 then
			GuardBot.PrintToChat("GuardBot Error: "..color[3].." is not a valid color component. Value must be greater than or equal to 0 and less than 256.", 3)

		
			return
		end
		
		GuardBot.Settings.iconColorR = color[1]		
		GuardBot.Settings.iconColorG = color[2]
		GuardBot.Settings.iconColorB = color[3]
		
		GuardBot.SetupIconWindow()
		
		GuardBot.PrintToChat("GuardBot icon color has been changed.", 4)
		
		return
		
	elseif command == "setalpha" and value ~= nil then
		local alpha = tonumber(value)
		if alpha < 0 or alpha > 1.0 then
			GuardBot.PrintToChat("GuardBot Error: "..alpha.." is not a valid icon alpha component. Must be greater than or equal to 0 and less than or equal to 1.0.", 3)
		
			return
		end
		
		GuardBot.Settings.iconAlpha = alpha
		
		GuardBot.SetupIconWindow()
		
		GuardBot.PrintToChat("GuardBot icon alpha has been changed.", 4)
		
		return
		
	elseif command == "enablepartymsgs" then
		GuardBot.Settings.enablePartyMsgs = true
		
		GuardBot.PrintToChat("GuardBot will message your group when you guard a player.", 4)
		
		return
		
	elseif command == "disablepartymsgs" then
		GuardBot.Settings.enablePartyMsgs = false
		
		GuardBot.PrintToChat("GuardBot will not message your group when you guard a player.", 4)
		
		return
	end
end

function GuardBot.ShowHelp()
	GuardBot.PrintToChat("GuardBot Help:", 4)
	GuardBot.PrintToChat("/guardbot - Opens help menu.", 2)	
	GuardBot.PrintToChat("/guardbot enable/disable - Enables/disables GuardBot addon.", 2)
	GuardBot.PrintToChat("/guardbot showsettings - Displays current GuardBot settings.", 2)
	GuardBot.PrintToChat("/guardbot resetsettings - Resets GuardBot settings to default.", 2)
	GuardBot.PrintToChat("/guardbot setoffset x y - Sets the offset of the GuardBot icon along the X and Y axis.", 2)
	GuardBot.PrintToChat("/guardbot setscale value - Sets the scale of the GuardBot icon (value must be greater than 0; 0.5 = half size, 1 = normal size, 2 = double size, etc.).", 2)
	GuardBot.PrintToChat("/guardbot enablepartymsgs/disablepartymsgs - Enables/disables GuardBot to message your group when you guard someone.", 2)
end

function GuardBot.DumpSettings()
	GuardBot.PrintToChat("GuardBot Settings:", 4)
	GuardBot.PrintToChat("Enable Party Messages = "..tostring(GuardBot.Settings.enablePartyMsgs), 2)
	GuardBot.PrintToChat("Icon Offset X = "..GuardBot.Settings.iconOffsetX, 2)
	GuardBot.PrintToChat("Icon Offset Y = "..GuardBot.Settings.iconOffsetY, 2)
	GuardBot.PrintToChat("Icon Scale = "..GuardBot.Settings.iconTexScaleMult, 2)
	GuardBot.PrintToChat("Icon Color = "..GuardBot.Settings.iconColorR.." "..GuardBot.Settings.iconColorG.." "..GuardBot.Settings.iconColorB, 2)
	GuardBot.PrintToChat("Icon Alpha = "..GuardBot.Settings.iconAlpha, 2)
end

function GuardBot.PrintToChat(string, colorId)
	--colorId: 0=SAY filter (white), 1=TELL_RECEIVE filter (blue), 2=TELL_SEND filter (blue), 3=undefined filter (yellow?), 4=GROUP filter (green)
	TextLogAddEntry("Chat", colorId, towstring(string))
end

--Little helper method to split the params out of the slash command value. Capable of using multiple delimiter types. Returns a table with each 
--param mapped to a single index.
function GuardBot.SplitSlashParam(str, delim, max)
	--ignore bad params
	if string.find(str, delim) == nil then
	return { str }
	end

	if max == nil or max < 1 then
		max = 0
	end
	
	local result = {}
	local pat = "(.-)" .. delim .. "()"
	local nb = 0
	local lastPos

	for part, pos in string.gfind(str, pat) do
		nb = nb + 1
		result[nb] = part
		lastPos = pos
		
		if nb == max then break end
	end

	--handle the last field
	if nb ~= max then
		result[nb + 1] = string.sub(str, lastPos)
	end

	return result
end			
				

			
	