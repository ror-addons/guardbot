<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="GuardBot" version="1.06" date="11/08/2009" >
		<Author name="Arclighte" email="nowhere@nowhere.comnetorg" />
		<Description text="Automatically waypoints your guarded target." />
		<VersionSettings gameVersion="1.3.2" windowsVersion="1.0" savedVariablesVersion="1.03" />
		<WARInfo>
			<Categories>
				<Category name="RVR"/>
				<Category name="GROUPING"/>
				<Category name="COMBAT"/>
				<Category name="BUFFS_DEBUFFS"/>
			</Categories>
			
			<Careers>
				<Career name="BLACKGUARD"/>
				<Career name="IRON_BREAKER"/>
				<Career name="BLACK_ORC"/>
				<Career name="KNIGHT"/>
				<Career name="SWORDMASTER"/>
				<Career name="CHOSEN"/>
			</Careers>
		</WARInfo>
		
		<Dependencies>
			<Dependency name="LibSlash" optional="true" forceEnable="false"/>
		</Dependencies>
		
		<Files>
			<File name="GuardBot.xml" />		
			<File name="GuardBot.lua" />
			<File name="TargetInfoFix.lua" />
		</Files>
		
		<SavedVariables>
			<SavedVariable name="GuardBot.Settings"/>
		</SavedVariables>
				
		<OnInitialize>
			<CallFunction name="GuardBot.Initialize" />
			<CreateWindow name="GuardBotIconWindow" show="false"/>
		</OnInitialize>
		
		<OnShutdown>
			<CallFunction name="GuardBot.Shutdown" />
		</OnShutdown>
		
		<OnUpdate>
			<CallFunction name="GuardBot.Update" />
		</OnUpdate>

	</UiMod>
</ModuleFile>